extends PanelContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(1,13):
		var binding		= $OneBinding.duplicate()
		
		binding.show()
		binding.initActions(i)
		$NoteBindings.add_child(binding)
		 
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

