extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var fold = 0 #the octave
var velocity = 0
var channel = 1 #should be 1- felt like parametrizing it wasnt that useful
# Called when the node enters the scene tree for the first time.

signal midiMessage(timestamp,header,message)

var scale = Scales.defaultScale

func setScale(new_scale):
	fold *= float(scale.span) / float(new_scale.span)
	scale = new_scale
	

func _input(event):
	if event is InputEventKey:
		var size = scale.notes.size()
		var note = 0
		var header
		for i in range(1,size+1):
			if event.is_action_pressed("note_"+str(i)):
				note = MidiUtils.buildNoteMidi(i-1,fold,scale.span,scale.notes,scale.offset)
				header =  [MidiLib.MidiKind.NoteOn, channel]
				if event.shift:
					velocity= 100
				elif event.control:
					velocity = 15
				else:
					velocity =63
				emit_signal("midiMessage",
					0, #timestamp -- later get actual timestamp ?
					header,
					PoolByteArray([
						CONSTS.MIDICONSTS.KeyboardMIDI, #we can give an abitrary value because the engine uses the header instead of the status byte
						note,
						velocity
					]))
				break
			elif event.is_action_released("note_"+str(i)):
				note = MidiUtils.buildNoteMidi(i-1,fold,scale.span,scale.notes,scale.offset)
				header =  [MidiLib.MidiKind.NoteOff, channel] 
				emit_signal("midiMessage",
					0, #timestamp -- later get actual timestamp ?
					header,
					PoolByteArray([
						CONSTS.MIDICONSTS.KeyboardMIDI, #we can give an abitrary value because the engine uses the header instead of the status byte
						note,
						0 #we can just give 0 here as the 3rd byte is not used for note offs
					]))
				break
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
