extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum MidiKind {
	NoteOff,
	NoteOn,
	NoteKeyPressure,
	SetParameter,
	SetProgram,
	ChangePressure,
	PitchBend,
	MetaEvent
   }

var midiKindNames = [
	"NoteOff",
	"NoteOn",
	"NoteKeyPressure",
	"SetParameter",
	"SetProgram",
	"ChangePressure",
	"PitchBend",
	"MetaEvent"]

func showMidiKind(kind):
	if kind < midiKindNames.size():
		return midiKindNames[kind]
	else:
		return "Other"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
