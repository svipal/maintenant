extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

class Scale :
	var name : String
	var span: int #the span of the scale
	var offset : int  #the offset in semitones if you're using standard tuning
	var notes : PoolByteArray #which notes the scale contains, in standard tuning
	var note_names : PoolStringArray

	func _init(n,sp,of,no,non):
		name = n
		span = sp
		offset = of
		notes = no
		note_names = non
	
	
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#C
var defaultScale  = Scale.new(
	"FullChromaScale",
	12,0,[0,1,2,3,4,5,6,7,8,9,10,11]
	,["do","do#","re","re#","mi","fa","fa#","sol","sol#","la","la#","si"])
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
