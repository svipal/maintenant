extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var waitForMidiX = false
var waitForMidiY= false
var JoyX = MidiLib.MidiKind.PitchBend #default is pitch wheel
var JoyY = MidiLib.MidiKind.SetParameter # default is Mod wheel
var JoyXCCNum = 0 #not used if pitchbend
var JoyYCCNum = 1 #cc number

signal waitForX
signal waitForY
# Called when the node enters the scene tree for the first time.
func _ready():
     MidiHandler.connect("midiMessage", self,"_on_MIDI_Message_received")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass


func _on_ButtonX_toggled(button_pressed):
    print("wait for X")
    MidiHandler.waitAxisXCC()
    waitForMidiX = true
    pass # Replace with function body.


func _on_ButtonY_toggled(button_pressed):
    print("wait for Y")
    MidiHandler.waitAxisYCC()
    waitForMidiY = true
    pass # Replace with function body.

func _on_MIDI_Message_received(timestamp,header, message):
    match header[0]:
    #Normally, The only way you would get the 2 following messages is after a wait for cc
    #Right now this doesn't work at all and you should rather enter the cc number manually.
        MidiLib.MidiKind.PitchBend:
            if waitForMidiX:
                $VBC/XC/HBC/CCNumX.text = "PitchBend"
                $VBC/XC/HBC/ButtonX.pressed = false
                waitForMidiX = false
            elif waitForMidiY:
                $VBC/YC/HBC/CCNumY.text = "PitchBend"
                $VBC/YC/HBC/ButtonY.pressed = false
                waitForMidiY = false
        MidiLib.MidiKind.SetParameter:
            if waitForMidiX:
                $VBC/XC/HBC/CCNumX.text = "CC" + str(message[1])
                $VBC/XC/HBC/ButtonX.pressed = false
                waitForMidiX = false
            elif waitForMidiY:
                $VBC/YC/HBC/CCNumY.text = "CC" + str(message[1])
                $VBC/YC/HBC/ButtonY.pressed = false
                waitForMidiY = false
    
func _on_CheckBox_toggled(button_pressed):
    MidiHandler.setFlipX(not button_pressed)
    pass # Replace with function body.


func _on_CheckBox2_toggled(button_pressed):
    MidiHandler.setFlipY(button_pressed)
    pass # Replace with function body.


func _on_ConfirmX_pressed():
    var ccn = int($VBC/XC/HBC/CCNumX.text)
    MidiHandler.setAxisXCC(ccn)
    pass # Replace with function body.


func _on_ConfirmY_pressed():
    var ccn = int($VBC/YC/HBC/CCNumY.text)
    MidiHandler.setAxisYCC(ccn)
    pass # Replace with function body.


