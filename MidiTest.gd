extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _KeysMenu       = preload("res://ChooseKeys.tscn")
var _MidiMenu       = preload("res://MidiMenu.tscn")

onready var KeysMenu = _KeysMenu.instance()
onready var MidiMenu = _MidiMenu.instance()
#-- nodes
onready var joystickLbX = $MHBC/JoyStick/CenterContainer/VSplitContainer/LabelX
onready var joystickLbY = $MHBC/JoyStick/CenterContainer/VSplitContainer/LabelY
onready var joystickSp = $MHBC/JoyStick/CenterContainer/VSplitContainer/JoyStick2/Sprite

onready var playButton = $FMODparamscontainer/hbc/hbc2/Play
onready var synthButton = $fsparamscontainer/hbc/hbc2/Synth

var ports : PoolStringArray
var connected = false
#--input

var kJoyPos = Vector2(0.0,0.0)
var joyPos = Vector2(0.0,0.0)

#-- midi channel (1-16) sent to the midi handler if you prefer overriding it from godot than from your controller/system
var midi_channel = 1
func channel_set():
	return $MHBC/VSC/HBC/OverrideCB.pressed

# MidiHandler, SoundEngine and InputHandler are gdnative rust libs, and autoloaded singleton.
func connectSignals():
	SoundEngine.connect("Step Notif", self, "_on_FMOD_notif")
	SoundEngine.connect("Update Failed", self, "_on_FMOD_notif")
	SoundEngine.connect("FMOD System Init", self, "_on_fmod_system_init")
	SoundEngine.connect("FS System Init", self, "_on_fs_system_init")
	SoundEngine.connect("Prog Change", self, "_on_prog_change")
	SoundEngine.connect("Beat", self, "_on_beat")
	SoundEngine.connect("Beat", $MHBC/VSC/Roll, "_on_beat")
	SoundEngine.connect("Note Precision", self, "_on_note")

	InputHandler.connect("midiMessage", self,"_on_MIDI_Message_received")

	MidiHandler.connect("portClosed", self,"_on_MIDI_Port_closed")
	MidiHandler.connect("portConnected", self,"_on_MIDI_Port_connected")
	MidiHandler.connect("midiMessage", self,"_on_MIDI_Message_received")
	
var settings = {
	"fs_periodSize":224,
	"fs_audiodriver":"jack",
	"fs_cores": 4,
	"FMOD_channels":128}

func loadFont(fname):
	var instrnames = SoundEngine.loadFont(fname)
	$fsparamscontainer/hbc/vbc/HBoxContainer/FontName.text = fname
#	var instrnames = SoundEngine.loadFont("Craftsynth")
	for name in instrnames:
		$fsparamscontainer/hbc/vbc/HBoxContainer/instrument.add_item(name)

func _ready():
	connectSignals()
	SoundEngine.initSystem(settings)
	#fs stuff
	loadFont("ChoriumRevA")
	#FMOD stuff
	SoundEngine.loadBank("assets/banks/Desktop/Master.bank")
	SoundEngine.loadBank("assets/banks/Desktop/Master.strings.bank")
	SoundEngine.loadEvent("event:/Souliers")
	SoundEngine.createEventInstance("event:/Souliers")
	#input stuff
	InputHandler.linkWithSoundEngine(SoundEngine,MidiHandler)
	InputHandler.loadScale(
		Scales.defaultScale.span,
		Scales.defaultScale.notes,
		Scales.defaultScale.offset)
	InputHandler.setFold(5)
	refreshPorts()
	
	
	#notes stuf
	pass # Replace with function body.
#    $MHBC/VSC/HBC/CheckBox.pressed = 1

func refreshPorts():
	ports = MidiHandler.listPorts()
	print("open midi inputs :")
	$MHBC/VSC/HBC/OptionButton.clear()
	for name in ports:
		print("  •"+name)
		$MHBC/VSC/HBC/OptionButton.add_item(name)

func _input(event : InputEvent):
	if event.is_action_pressed("ui_down"):
		kJoyPos.y = 1
	elif event.is_action_pressed("ui_up"):
		kJoyPos.y = -1
	elif event.is_action_pressed("ui_left"):
		kJoyPos.x = -1
	elif event.is_action_pressed("ui_right"):
		kJoyPos.x = 1
	if event.is_action_released("ui_down"):
		kJoyPos.y = 0
	elif event.is_action_released("ui_up"):
		kJoyPos.y = 0
	elif event.is_action_released("ui_left"):
		kJoyPos.x = 0
	elif event.is_action_released("ui_right"):
		kJoyPos.x = 0

#we actually don't need that function
func _on_OptionButton_item_selected(index):
	pass 

# Replace with function body.
func _on_MIDI_Message_received(timestamp,header, message):
	match header[0]:
		MidiLib.MidiKind.SetProgram:
			$fsparamscontainer/hbc/vbc/instrument.select(message[1])
			print("pc change: ",message[1])
		MidiLib.MidiKind.NoteOn:
			var noteInfo = MidiUtils.getNoteIndex(message[1], 12, 0)
			var note  = noteInfo[0]
			var octave = noteInfo[1]
			$MHBC/VSC/Roll.activateNote(note, octave)
		MidiLib.MidiKind.NoteOff:
			var noteInfo = MidiUtils.getNoteIndex(message[1], 12, 0)
			var note  = noteInfo[0]
			$MHBC/VSC/Roll.deactivateNote(note)
func _on_MIDI_Port_connected(success):
	if success:
		print("successfully connected to selected port")
		connected=true
	else:
		print("couldn't connect to port")
		$MHBC/VSC/HBC/ConnectButton.pressed = 0
	$MHBC/VSC/HBC/CheckBox.pressed = success
	
func _on_MIDI_Port_closed(success):
	if success:
		print("port successfully closed")
		$MHBC/VSC/HBC/ConnectButton.pressed = 0
		$MHBC/VSC/HBC/CheckBox.pressed = 0
	else:
		print("couldn't close port")
		
	
func _on_Label_toggled(button_pressed):
	if button_pressed:
		if ports.size()!=0:
			print("trying connection")
			var index = $MHBC/VSC/HBC/OptionButton.selected 
			MidiHandler.selectPort(index)
			
	else:
		if connected:
			MidiHandler.closePort()
			connected=false
	pass # Replace with function body.

func _process(_delta):
#	SoundEngine.update()
	var x = MidiHandler.getStickX() 
	var y = MidiHandler.getStickY()
	joyPos=Vector2(x,y)
	joystickSp.position = (kJoyPos+joyPos)*50 + Vector2(50,50)
	joystickLbX.text = str(joyPos.x + kJoyPos.y)
	joystickLbY.text = str(joyPos.y + kJoyPos.y)
#    print(joyPos)

func _on_Keyboard_toggled(button_pressed):
	if button_pressed:
		KeysMenu.rect_position=$MHBC/VSC/HBC/Keyboard.rect_position + Vector2(20,20)
		$KeyContainer.add_child(KeysMenu)
	else:
		$KeyContainer.remove_child(KeysMenu)
	pass # Replace with function body.


func _on_fmod_system_init():
	print("FMOD system initialized")

func _on_fs_system_init():
	print("fluidsynth system initialized")

func _on_FMOD_notif(msg):
	print(msg)
	if msg=="samples loaded":
		playButton.disabled=false

func _on_MIDIOptions_toggled(button_pressed):
	if button_pressed:
		MidiMenu.rect_position=$MHBC/VSC/HBC/MIDIOptions.rect_position + Vector2(20,20)
		$MidiContainer.add_child(MidiMenu)
	else:
		$MidiContainer.remove_child(MidiMenu)
	pass # Replace with function body.

func _on_Play_toggled(press):
	if press:
		playButton.text = "STOP"
		SoundEngine.playCurrentInstance()
	else:
		playButton.text = "PLAY"
		SoundEngine.stopCurrentInstance(1)
	pass # Replace with function body.

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		SoundEngine.clean()
		get_tree().quit() # default behavior

func _on_phase_value_changed(value):
	SoundEngine.setLocalParameter("phase",
									value,
									false)

func _on_intensity_value_changed(value):
	SoundEngine.setLocalParameter("intensity",
									value,
									false)
func _on_beat(beat,pos,bar, _tupper, _tlower, tempo):
#	print(beat,"-",pos,"-",bar)
	pass

func _on_note(precision):
#	print(precision)
	pass

func _on_Synth_toggled(press):
	if press:
		synthButton.text = "SYNTH"
		SoundEngine.startSynth()
	else:
		synthButton.text = "SYNTH?"
		SoundEngine.stopSynth()
	pass # Replace with function body.

func _on_Refresh_pressed():
	$MHBC/VSC/HBC/ConnectButton.pressed = false
	MidiHandler.closePort()
	refreshPorts()
	pass # Replace with function body.




func _on_Channel_item_selected(index):
	midi_channel = index
	if $MHBC/VSC/HBC/OverrideCB.pressed:
		MidiHandler.setChannel(midi_channel)


func _on_OverrideButton_toggled(button_pressed):
	if button_pressed:
		$MHBC/VSC/HBC/OverrideCB.pressed=true
		MidiHandler.setChannel(midi_channel)
	else:
		$MHBC/VSC/HBC/OverrideCB.pressed=false
		MidiHandler.releaseChannel()

	pass # Replace with function body.


func _on_instrument_item_selected(index):
	SoundEngine.progChange(midi_channel, index)
	pass # Replace with function body.


func _on_gainslider_value_changed(value):
	SoundEngine.setFSGain(value)
	pass # Replace with function body.
	
func _on_prog_change(channel,prog):
	print("program changed to "+str(prog)+" on channel "+str(channel))
#	$fsparamscontainer/hbc/vbc/instrument.select(prog)
