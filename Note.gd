extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var handler
onready var line = $NoteLine
onready var emitter = $NoteCreation
var pos = -12
var ended = false
var div = 96
var note = 0
var velo = 0
var time = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	self.position.x =500
	pass # Replace with function body.

func setNote(time,anote,velo):
	note = anote
	self.position.y = note * 21 + 10.5
	pos  = -time
	print("note position : " +str(pos))
	note = note
	$NoteLine.width=velo/10


func snapToNextCell():
	print("note ",note,"currently at", pos)
	pos = clamp(pos +1,-100000000,12)
	if not ended:
		$NoteLine.points[1].x = -(pos+12)*50
	else:
		$NoteCreation.emitting=false
		$NoteLine.points[0].x= -(pos+12)*50
	if pos >-12:
		show()
func endNote():
	ended = true

func _process(delta):
	if handler:
		if not ended:
			$NoteLine.points[1].x-= handler.tempo*delta*0.8
		else:
			$NoteLine.points[0].x-= handler.tempo*delta*0.8

		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
