extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tempo = 0
var div = 96 setget setResolution#default midi resolution
var beginNotes = [] setget setBNotes
var endNotes = [] setget setENotes

var currentB = []
var currentE = []
var scaleSize = 0

func setResolution(newdiv):
	div = newdiv

func setENotes(notes):
	endNotes = notes
	
func setBNotes(notes):
	beginNotes = notes

# Called when the node enters the scene tree for the first time.
func _ready():
	
#	$Note.material.set_shader_param("TextureUniform", $TextureRect.texture)
	pass # Replace with function body.

func createNotes():
	for note in beginNotes:
		var newNote = $Note.duplicate()
		newNote.div = div
		newNote.handler = self
		var noteIndex = MidiUtils.getNoteIndex(note[1],scaleSize,0)
		newNote.setNote(note[0],noteIndex[0],note[2])
		$notes.add_child(newNote)
	for note in endNotes:
		
func setScaleSize(size):
	scaleSize = size
	
	
func beat():
	# first we snap the old notes to place
	for noteRepr in $notes.get_children():
		noteRepr.snapToNextCell()
	
	
#	for note in beginNotes:
		
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
