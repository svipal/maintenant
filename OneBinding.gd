extends HBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var waitForB1 = false
var waitForB2 = false

var actionBindings : Array #of InputEvents
var noteNum = 0
var noteName = ""
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func initActions(num):
	noteNum = num
	noteName = "note_"+str(noteNum)
	$NoteName.text = noteName
	actionBindings	= InputMap.get_action_list(noteName)
	print(actionBindings)
	if actionBindings.size() < 1:
		$Button1.text = "Unbound"
		$Button2.text = "Unbound"
	elif actionBindings.size() < 2:
		$Button1.text = actionBindings[0].as_text()
		$Button2.text = "Unbound"
	else:
		$Button1.text = actionBindings[0].as_text()
		$Button2.text = actionBindings[1].as_text()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _input(event):
	if waitForB1:
		if event is InputEventKey:
			InputMap.action_add_event("note_"+str(noteNum),
										event)
			$Button1.pressed=false
			waitForB1 = false
		initActions(noteNum)
	elif waitForB2:
		if event is InputEventKey:
			InputMap.action_add_event("note_"+str(noteNum),
										event)
			$Button2.pressed=false
			waitForB2 = false
		initActions(noteNum)
func _on_Button1_toggled(button_pressed):
	if button_pressed:
		if !actionBindings.empty():
			InputMap.action_erase_event("note_"+str(noteNum),
										actionBindings[0])
		waitForB1= true
	pass # Replace with function body.


func _on_Button2_toggled(button_pressed):
	if button_pressed :
		if actionBindings.size() > 1:
			InputMap.action_erase_event("note_"+str(noteNum),
										actionBindings[1])
		waitForB2= true
	pass # Replace with function body.
