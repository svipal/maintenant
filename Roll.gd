extends HSplitContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var scale = ["do"
			,"do#"
			,"re"
			,"re#"
			,"mi"
			,"fa"
			,"fa#"
			,"sol"
			,"sol#"
			,"la"
			,"la#"
			,"si"
			]

func createScale(scale):
	for name in scale:
		var note = $Roll/NoteContainer.duplicate()
		note.get_child(0).get_child(0).text = name
		note.show()
		$Roll/Notes.add_child(note)
	createGrid(scale,5)


func deactivateNote(note):
	$Roll/Notes.get_child(note).self_modulate=Color(1,1,1,1)
	$Roll/Notes.get_child(note).get_child(0).get_child(1).text = ""
	
func activateNote(note,octave):
	$Roll/Notes.get_child(note).self_modulate=Color(
		float(octave % 3), 
		pow(octave % 4, 2.0) + 0.1,
		1 - pow(octave % 7,1.5)/8.0, 1)
	$Roll/Notes.get_child(note).get_child(0).get_child(1).text = str(octave-1)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	$NotesEmitter.position = $Grid.rect_position
	createScale(scale)
	# let's get then notes from the first file.
	var res = MidiFileHandler.loadFile("test")
	if res:
		print ("file loaded successfully")
	else:
		print("nofileforu")
	var notes = MidiFileHandler.getNotes("test",1)
	print("notes in file :",notes[1].size())
	for note in notes[0]:
		print("fuck")
		print (note[1])
	var resolution = MidiFileHandler.getDiv("test")
	$NotesEmitter.setResolution(resolution)
	$NotesEmitter.setBNotes(notes[0])
	$NotesEmitter.setENotes(notes[1])
	
	$NotesEmitter.setScaleSize(scale.size())
	$NotesEmitter.createNotes()
	pass # Replace with function body.

var zoomX = 1.0
var zoom = 1.0
var gridTexture = ImageTexture.new()

func createGrid(scale, beats):
	var format = get_viewport().get_texture().get_data().get_format()
	var rawGrid = Image.new()
	var width = 100.0
	var height = 21.0
	rawGrid.create(beats*width*zoomX*zoom, zoom*scale.size()*height, false, format)
	rawGrid.fill(Color(1,0.2,0.3,1))
	rawGrid.unlock()
	
	var margin = 0.1
	var cell = Image.new()
	var cellW = (width-margin*2)*zoomX*zoom
	var cellH = zoom*height-margin*2
	cell.create(cellW, cellH, false,format)
	for x in range(0,beats):
		for y in range(0,scale.size()):
			rawGrid.blit_rect(cell,
				Rect2(Vector2(0,0),Vector2(cellW,cellH)),
				Vector2(x*width+margin,y*height+margin))
	rawGrid.lock()
	
	gridTexture.create_from_image(rawGrid,format)
	$Grid/TR.texture=gridTexture
	$Grid/TR.rect_min_size= Vector2(beats*width, scale.size()*height+5)
	print($Grid/TR.texture)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var beat = SoundEngine.getBeat()
	var u = float(beat)/4.0
	var mod = SoundEngine.getBeatPower()/100.0
	modulate = Color(u,1-u,u*u,1)/2*mod + Color.white


func _on_beat(beat,pos,bar, _tupper, _tlower, tempo):
	$NotesEmitter.tempo = tempo
	$NotesEmitter.beat()
